/*
Практикум 2, задание 2
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{

	int number = 0, inNumber = 0;
	srand(time(0));
	number = rand()%100;
	printf("I thought a number. Guess what? \nEnter your guess: ");
	
	do
	{
		fflush(stdin);
		if (scanf("%d", &inNumber) == 0) {printf("Incorrect input! Try again!\n"); continue;}
		
		if (inNumber < number) printf("More\n");
		else if (inNumber > number) printf("Less\n");
		
		
	}
	while(inNumber != number);
	printf("Exactly!\n");
	return 0;
}