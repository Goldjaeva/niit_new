/*
практикум 2, задание 7
*/

#include <stdio.h>

int main()
{
	char str[256], symbols[256];
	int counter[256] = {0};
	int i = 0, j = 0;
	int count = 0;
	int repiting = 0;

	printf("Enter a string: ");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = '\0';

	for (i = 0; i < strlen(str); i++)
	{
		j = 0; repiting = -1;
		do
		{
			if (str[i] == symbols[j])
				repiting  = j;
			j++; 
		}
		while (j < count && repiting < 0);

		if (repiting >= 0) 
			counter[repiting]++;
		else
		{
			symbols[count] = str[i];
			counter[count]++;
			count++;
		
		}
		
	}
	
	i = 0;
	for (i = 0; i < count ; i++)
		printf("%c - %d\n", symbols[i], counter[i]);
	return 0;
}