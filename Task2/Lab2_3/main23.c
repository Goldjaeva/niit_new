/*
Практикум 2, задание 3
*/

#include <stdio.h>

int main()
{
	int count = 0;
	int i = 0, j = 0;

	do
	{
	printf("Enter row's count: ");
	fflush(stdin);
	if (scanf("%d", &count) == 0) printf("Incorrect input!");
	}
	while (count == 0 || count > 40);

	for (i = 0; i < count; i++)
	{
		for (j = 0; j < count - 1 - i; j++)
			printf(" ");

		for (j = 0; j < 2 * i + 1; j++)
			printf("*");

		printf("\n");
	}
	return 0;
}