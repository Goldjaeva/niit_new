/*--------------
Практикум 2, задание 6
------------------*/

#include <stdio.h>
#include <string.h>

int main()
{
	char str[256], ch;
	int i = 0, l = 256, j = 0, f = 1;

	printf("Enter a string: ");
	fgets(&str, 256, stdin);
	l = strlen(str);
	
	//multiple spaces
	ch = str[0];
	while (f)
	{
		f = 0;
		for (i = 1; i < l; i++)
		{
			if (ch == ' ' && str[i] == ' ')
			{
				for (j = i; j < l - 1; j++)
				{
					str[j] = str[j + 1];
				}
				l--;
				f++;
			}
			ch = str[i];
		}
	}

	//space at the beginning
	if (str[0] == ' ')
	{
		for (j = 0; j < l - 1; j++)
				{
					str[j] = str[j + 1];
				}
				l--;
	}

	//space at the end
	if (str[l - 2] == ' ')
	{
		str[l - 2] = '\0';
		l--;
	}
	str[l - 1] = '\0';

	printf("%s", str);
	return 0;
}