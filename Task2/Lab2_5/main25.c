/*--------------
Практикум 2, задание 5
------------------*/

#include <stdio.h>
#include <time.h>

int main()
{
	char passwd[9];
	int i = 0, j = 0, group = 0;
	srand(time(0));

	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 8; j++)
		{
			group = rand() % 3;
			switch (group)
			{
			case 0:
				{
				passwd[j] = '0' + rand() % 10;
				break;
				}
			case 1:
				{
				passwd[j] = 'a' + rand() % 26;
				break;
				}
			case 2:
				{
				passwd[j] = 'A' + rand() % 26;
				break;
				}
			}
			
		}
		passwd[j] = '\0';
		printf("%s\n", passwd);
	}

	return 0;
}