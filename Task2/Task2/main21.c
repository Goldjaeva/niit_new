/*----------
Практикум 2, задание 1
-------------*/

#include <stdio.h>
#include <time.h>

int main()
{
	int t = 1;
	float h = 0,  H = 0;
	const float g = 9.81f;
	clock_t begin = clock();
	
	printf("Enter a height: ");
	if (!scanf("%f", &H) || H < 0)
	{
		printf("Incorrect value!\n");
		return 1;
	}

	h = H;
	while (h > 0)
	{
		printf("t = %dc\th = %.1fm\n\a", t - 1, h);
		h = H - g * t * t / 2;
		t++;
		begin = clock();
		while (clock() < begin + 1000);
	}
	printf("BABAH!\n");
	return 0;
}