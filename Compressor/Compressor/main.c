 #include <stdio.h>
#include "structure.h"

int MakeTable(char * symbols, int *counter, int *m);
void FillData(struct SYM *s, char *symb, int * counter, int n, int m);
void Sort(char *s, int value[], const int n);
struct SYM* buildTree(struct SYM *psym[], int N);
void makeCodes(struct SYM *root);
void CodeFile(struct SYM * syms, int count);
void pack(int count);
char * makeHeader(struct SYM * syms, int N, int m);




 int main()
 {
	int counter[256] = {0};
	char symb[256];
	int i = 0;
	int n = 0, m = 0;
    struct SYM syms[256];
	struct SYM *psyms[256];
	struct SYM tmp;
	
	n = MakeTable(symb, counter, &m);

	
	m--;
	n--;
	Sort(symb, counter, n);
	printf("%d\n", m);
	FillData(syms, symb, counter, n, m);
	
	for (i = 0; i < n; i++)
	{
		psyms[i] = &syms[i];
	}
	psyms[0] = buildTree(psyms, n);
	makeCodes(psyms[0]);
	tmp = *psyms[0];
 	
	CodeFile(syms, n);
	
//	makeHeader(syms, n);
	pack(makeHeader(syms, n, m));
	for (i = 0; i < n; i++)
	{
		printf("%c - %s\n", syms[i].ch, syms[i].code);
	}
 	return 0;

 }



   