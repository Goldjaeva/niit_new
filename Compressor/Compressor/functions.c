#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "structure.h"

int MakeTable(char * symbols, int *counter, int *m)
{
	
 	int i = 0, j = 0;
 	int count = 0;
 	int repiting = 0;
	char ch = 0;
	
	FILE * fp = fopen("file.txt", "rb");
	
	if (fp != NULL)
	
	while (ch != EOF)
	{
		(*m)++;
 		ch = fgetc(fp);
 		j = 0; 
		repiting = -1;
 		
		do
 		{
 			if (ch == symbols[j])
 				repiting = j;
 			j++; 
 		}
 		while (j < count && repiting < 0);
 		
		if (repiting >= 0) 
 			counter[repiting]++;
 		else
 		{
 			symbols[count] = ch;
 			counter[count]++;
 			count++;
 		}
 	i = 0;
	}

	return count;
}

void FillData(struct SYM *s, char *symb, int * counter, int n, int m)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		s[i].ch = symb[i];
		s[i].code[0] = '\0';
		s[i].freq = (double)counter[i] / (double)m;
		s[i].left = 0;
		s[i].right = 0;
	}

}

void Sort(char *s, int value[], const int n)
{
	int max = 0;
	int iMax = 0;
	int i = 0, j = 0;
	char *tmp;
	int a;

	for (i = 0; i < n; i++)
	{
		max = 0;
		iMax = i;
		for (j = i; j < n; j++)
		if (value[j] > max)
		{
			max = value[j];
			iMax = j;
		}

		a = value[iMax];
		value[iMax] = value[i];
		value[i] = a;

		tmp = s[iMax];
		s[iMax] = s[i];
		s[i] = tmp;
	}
}

struct SYM* buildTree(struct SYM *psym[], int N)
{
	struct SYM *temp=(struct SYM*)malloc(sizeof(struct SYM));
	int i = 0, place = 0;
	temp->freq=psym[N-2]->freq+psym[N-1]->freq;
	temp->left=psym[N-1];
	temp->right=psym[N-2];
	temp->code[0]=0;
	if(N==2) 
		return temp;

	while (psym[i]->freq > temp->freq)
	{
		i++;
	}

	place = i;
	for (i = N; i > place; i--)
	{
		psym[i] = psym[i - 1];
	}
	psym[place] = temp;
return buildTree(psym,N-1);
}

void makeCodes(struct SYM *root)
{
	if(root->left)
	{
		strcpy(root->left->code,root->code);
		strcat(root->left->code,"0");
		makeCodes(root->left);
	
	}
	if(root->right)
	{
		strcpy(root->right->code,root->code);
		strcat(root->right->code,"1");
		makeCodes(root->right);
	}
}

void CodeFile( struct SYM * syms, int count)
{
	int ch; 
	int i;
	FILE *fp_101 = fopen("code.txt", "w+b");
	FILE *fp_in = fopen("file.txt", "rb");

	while((ch=fgetc(fp_in))!=-1)
	{
		for(i = 0; i < count; i++)
		{
			if(syms[i].ch==(unsigned char)ch) 
			{
				fputs(syms[i].code,fp_101); 
				break; 
			}
		}
	}
	fclose(fp_101);
	fclose(fp_in);
}

void pack(int count)
{
	FILE *codeFile = fopen("code.txt", "rb");
	FILE *pack = fopen("pack.txt", "ab");
	union CODE code;
	char buf[8] = {0};
	int i = 0;
	int ch = 0;

	for (i = 0; i < count / 8; i++)
	{
		fgets(buf, 8, codeFile);
		code.byte.b1=buf[0]-'0';
		code.byte.b2=buf[1]-'0';
		code.byte.b3=buf[2]-'0';
		code.byte.b4=buf[3]-'0';
		code.byte.b5=buf[4]-'0';
		code.byte.b6=buf[5]-'0';
		code.byte.b7=buf[6]-'0';
		code.byte.b8=buf[7]-'0';
		fprintf(pack, "%c", code.ch);
	}
	fclose(codeFile);
	fclose(pack);
}

int makeHeader(struct SYM * psyms, int count, int m)
{
	int i = 0, j = 0;
	FILE * fp = fopen("pack.txt", "wb");
	int tail = 0;
	FILE *code = fopen("code.txt", "rb");
	int ch = 0;


	fwrite("compresed", 1, 9, fp);
	fwrite(&count, 1, 1, fp);
	
	for (i = 0; i < count; i++)
	{
		fwrite(&psyms[i].ch, 1, 1, fp);
		fwrite(&psyms[i].freq, sizeof(float), 1, fp);
	}
	
	while (ch != -1)
	{
		ch = fgetc(code);
		count++;
	}

	tail = count % 8;
	if (tail != 0)
	{
		fclose(code);
		code = fopen("code.txt", "ab");
	
		for (i = 0; i < 8 - tail; i++)
		{
			fputc('0', code);
		}
		fclose(code);
	}

	fwrite(&tail, 1, 1, fp);
	fwrite(&m, 1, 1, fp);
	fwrite("txt", 1, 3, fp);

	fclose(fp);

	return count;
}
