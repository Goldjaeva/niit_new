#include <stdio.h>
#include <math.h>

#define n 29
#define m 29

int Form(char (*arr)[n], int N, int x, int y);

int main()
{
	char str[n][m];
	int i = 0, j = 0;

	for (i = 0; i < n; i++)
		for (j = 0; j < m; j++)
			str[i][j] = ' ';
	Form(str, 3, n/2 + 1, m/2 + 1);

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
			putchar(str[i][j]);
		putchar('\n');
	}
	return 0;
}

int Form(char (*arr)[n], int N, int x, int y)
{
	int k = pow(3, N-1);
	if (N != 1) 
	{

		Form(arr, N - 1,  x - k, y);
		Form(arr, N - 1,  x, y + k);
		Form(arr, N - 1,  x + k, y);
		Form(arr, N - 1,  x, y - k);
		Form(arr, N - 1,  x, y);

		
		N--;
	}
	if (N == 1) 
	{

		arr[x - 1][y] = '*';
		arr[x + 1][y] = '*';
		arr[x][y + 1] = '*';
		arr[x][y - 1] = '*';
		arr[x][y] = '*';
	}

	return N - 1;
	
}