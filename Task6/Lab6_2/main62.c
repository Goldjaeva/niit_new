#include <stdio.h>

int func(unsigned int n);
int main()
{
	unsigned int count = 1, maxn = 2, max = 0, n = 2;
	for (n = 2; n <= 1000000; n++)
	{
		count = func(n);
		if (count > max)
		{
			max = count;
			maxn = n;
		}

	}
	printf("Max n: %d\nCount of members: %d\n", maxn, max);
	return 0;
}

int func(unsigned int n)
{
	int count = 1;
	if (n > 1)
	{
		if (n % 2) count += func(3*n + 1); else count += func(n/2); 
	}
	else return count;
}