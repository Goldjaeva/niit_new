/*--------------------
��������� 1, ������ 3
---------------------*/

#include <stdio.h>

int main()
{
    char letter = ' ';
    float angle = 0.0f;
    const float pi = 3.1415926f;

    printf("Enter the angle: ");
    if (scanf("%f%c", &angle, &letter) != 2) 
    {
        printf("Incorrect format!\n");
        return 1;
    }
    
    if (letter == 'D' || letter == 'd')
    {
        printf("%gD = %gR\n", angle, pi * angle / 180);
    }
    else if (letter == 'R' || letter == 'r')
    {
        printf("%gR = %gD\n", angle, 180 * angle / pi);
    }
    else 
         printf("Incorrect format!\n");

    return 0;
}