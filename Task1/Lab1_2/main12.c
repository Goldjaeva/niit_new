/*--------------------
��������� 1, ������ 2
---------------------*/

#include <stdio.h>

int main()
{
	int hours, minutes, seconds;

	printf("What time is it? (hh:mm:ss)\n");
	if (scanf("%d:%d:%d", &hours, &minutes, &seconds) != 3) 
	{
		printf("Incorrect format!\n");
		return 1;
	}

	if (hours < 0 || hours > 23)
	{
		printf("Incorrect hour!\n");
		return 1;
	}

	if (minutes < 0 || minutes > 59)
	{
		printf("Incorrect minutes!\n");
		return 1;
	}

	if (seconds < 0 || seconds > 59)
	{
		printf("Incorrect seconds!\n");
		return 1;
	}
	

	if (hours >= 6 && hours < 12)
		printf("Good morning!\n");
	else if (hours >= 12 && hours <= 18)
		printf("Good day!\n");
	else if (hours >= 18 && hours <= 22)
		printf("Good evening!\n");
	else 
		printf("Good night!\n");

	return 0;
}	