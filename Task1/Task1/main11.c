/*--------------------
��������� 1, ������ 1
---------------------*/

#include <stdio.h>

int main()
{
	int height = 0, weight = 0, coeff = 100;
	char sex[2];

	printf("Enter your height: ");
	if (!scanf("%d", &height) || height <= 0 || height >= 300) 
	{
		printf("Incorrect value!\n");
		return 1;
	}
	
	printf("Enter your weight: ");
	if (!scanf("%d", &weight) || weight <= 0 || weight >= 1000)
	{
		printf("Incorrect value!\n");
		return 1;
	}

	printf("Enter your sex (f or m): ");
	
	if (!scanf("%s", &sex) || (sex[0] != 'f' && sex[0] != 'm'))
	{
		printf("Incorrect value!\n");
		return 1;
	}
	
	if (sex == 'f') 
		coeff = 110;

	if (height - coeff > weight)
	{
		printf("You need to eat!\n");
	}
	else if (height - coeff < weight)
	{
		printf ("You need to do more exercise!\n");
	}
	else
	{
		printf("You have a great form!\n");
	}
	return 0;
}