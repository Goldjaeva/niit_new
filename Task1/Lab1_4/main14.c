/*--------------------
��������� 1, ������ 4
---------------------*/

#include <stdio.h>

int main()
{
    int ft = 0, in = 0;
    float cm = 0;

    printf("Enter foots: ");
    if (!scanf("%d", &ft))
    {
         printf("Incorrect format!\n");
         return 1;
    }

    printf("Enter inchs: ");
    if (!scanf("%d", &in))
    {
         printf("Incorrect format!\n");
         return 1;
    }
    cm = in * 2.54 + ft * 12 * 2.54;
    printf("%dft %din = %.1fcm\n", ft, in, cm);

    return 0;
}