/*
Практикум 3, задание 3
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
char str[256];
char currentWord[256], maxWord[256];
	int i = 0, j =0;
	int max = 0;
	int inWord = 0;
	int lenght = 0;

	puts("Enter a string: ");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = ' ';

	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0) //if word's beginning
		{
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1) //if word's end
		{
			inWord = 0;
			currentWord[lenght] = '\0';
			if (lenght > max)
			{
				strcpy(maxWord, currentWord);
				max = lenght;
			}
			lenght = 0;
			printf(currentWord);
			printf("\n");
			currentWord[0] = '\0';
		}

		if (inWord == 1) 
		{
			currentWord[lenght++] = str[i];
		}
		else 
			lenght = 0;

		i++;
	}
	
	printf("\nThe longest word is \"%s\", its lenght is %d.\n", maxWord, max);
	return 0;
}