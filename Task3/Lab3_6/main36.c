/*
Практикум 3, задание 6
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define N 256

int main()
{
	int arr[N];
	int i = 0, iMin = 0, iMax = 0, max = 0, min = INT_MAX;
	long sum = 0;
	srand(time(0));

	for (i = 0; i < N; i++)
	{
			arr[i] = rand();
			printf("%d ", arr[i]);
	}

	for (i = 0; i < N; i++)
	{
		if (arr[i] > max)
		{
			max = arr[i];
			iMax = i;
		}

		if (arr[i] < min)
		{
			min = arr[i];
			iMin = i;
		}
	}
	
	printf("\nmax = %d, iMax = %d\nmin = %d, iMin = %d\n", max, iMax, min, iMin);
	
	if (iMin > iMax)
	{
			i = iMax;
			iMax = iMin;
			iMin = i;
	}

	for (i = iMin + 1; i < iMax; i++)
		sum += arr[i];
		
	printf("\nSum = %d\n", sum);

	return 0;
}