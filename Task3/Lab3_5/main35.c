/*
Практикум 3, задание 5
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define N 256

int main()
{
	int arr[N];
	int i = 0, iPlus = 0, iMinus = 0, number = 0;
	long sum = 0;
	srand(time(0));

	for (i = 0; i < N; i++)
	{
		number = rand();
		if (rand() % 2)
			arr[i] = number;
		else arr[i] = -number;
		printf("%d ", arr[i]);
	}

	while (arr[i] < 0) 
		i--;
	iPlus = i;
	i = 0;
	while (arr[i] > 0) 
		i++;
	iMinus = i + 1;

	for (i = iMinus; i < iPlus; i++)
		sum += arr[i];
		
	printf("\nSum = %d\n", sum);

	return 0;
}