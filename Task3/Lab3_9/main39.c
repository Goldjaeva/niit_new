/*
Практикум 3, задание 9
*/

#include <stdio.h>
#include <string.h>

int main()
{
	char str[256];
	char current = 0, symb = 0;
	int i = 1, len = 1, max = 0;

	puts("Enter a string: ");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = ' ';

	current = str[0];

	while (str[i])
	{
		if (str[i] == current)
		{
			len++;
		}
		else
		{
			if (len > max)
			{
				max = len;
				symb = current;
			}

			len = 1;
			current = str[i];
		}

		i++;
	}

	printf("Max sequence: ");
	for (i = 0; i < max; i++)
		printf("%c", symb );
	printf(", lenght - %d\n", max);

	return 0;
}