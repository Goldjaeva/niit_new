/* 
Практикум 3, задание 1
*/

#include <stdio.h>
#include <string.h>

int main()
{
	char str[256];
	int i = 0;
	int count = 0;
	int inWord = 0;

	puts("Enter a string: ");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = ' ';

	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0) 
		{
			count++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1) 
			inWord = 0;
		i++;
	}
	
	printf("Count of words: %d\n", count);
	return 0;
}