#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define m 20
#define n 20
void clear(char (*arr)[n], int M);
void form(char (*arr)[n], int M);
void copy(char (*arr)[n], int M);
void cls();
void print(char (*arr)[n], int M);



int main()
{
	char arr[n][m];
	clock_t begin = 0;

	while(1)
	{
		clear(arr, m);
		form(arr, m);
		copy(arr, m);
		print(arr, m);
		begin = clock();
		while (clock() < begin + 500);
		cls();
	}
	return 0;
}

void form(char (*arr)[n], int M)
{
	int i = 0, j = 0;

	for (i  = 0; i < n / 2; i++)
		for (j = 0; j < M / 2; j++)
		{
			if (rand() % 3 == 0)
				arr[i][j] = '*';
			else
				arr[i][j] = ' ';
		}

}

void print(char (*arr)[n], int M)
{
	int i = 0, j = 0;
	for (i = 0; i < n ; i++)
	{
		putchar('\n');
		for (j = 0; j < M; j++)
		{
			putchar(' ');
			putchar(arr[i][j]);
		}
	}
}

void clear(char (*arr)[n], int M)
{
	int i = 0, j = 0;

	for (i  = 0; i < n; i++)
		for (j = 0; j < M; j++)
		{
			arr[i][j] = ' ';
		}
}

void copy(char (*arr)[n], int M)
{
	int i = 0, j = 0;
	for (i = 0; i < n / 2; i++)
		for (j = 0; j < M / 2; j++)
		{
			arr[M - 1 - i][j] = arr[i][j];
			arr[i][n - 1 - j] = arr[i][j];
			arr[M - 1 - i][n - 1 - j] = arr[i][j];
		}
}

void cls()
{
	system("cls");
}