#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

void shaffle(char * pword, int n);
void findWord(char *pstr);

int main()
{
	char str[256];
	char ch[1];
	int i = 0, count = 0, len = 0, f = 0;
	FILE * fp = fopen("file.txt", "r");
	if (!fp)
	{
		perror("File error");
		return 1;
	}

	srand(time(0));

	while (!f)
	{
		i = 0;
		fgets(str, 256, fp);
		while (str[i] != '\n' && f == 0)
		{
			if (str[i] == 0)
				f = 1;
			i++;
		}
		str[i] = '\0';
		findWord(str);
		printf("%s\n", str);
	}
	return 0;
}

void findWord(char *pstr)
{
	int i = 0, inWord = 0, begin = 0;
	int n = 0;

	while (pstr[i])
	{
		n = 0;
		begin = i;
		while (pstr[i] != ' ' && pstr[i] != '\0')
		{
			i++;
			n++;
		}

		if (n > 3)
			shaffle(&pstr[begin], n);
		i++;
	}
}

void shaffle(char * pword, int n)
{
	int i = 0, j = 0;
	char tmp;

	for (i = 1; i < n - 1; i++)
	{
		j = rand() % i + 1;
		tmp = pword[i];
		pword[i] = pword[j];
		pword[j] = tmp;
	}

}