#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int printWord(const char * s);
int getWords(const char *str, char *p[]);

int main()
{
	char str[256];
	char *pwords[256] = {0};
	char *tmp;
	int N = 0, i = 0, word = 0, count = 0;
	printf("Enter a string, please:\n");
	fgets(str, 256, stdin);

	str[strlen(str) - 1] = '\0';
	
	srand(time(0));

	count = getWords(str, pwords);
	N = count; 
	for (i = 0; i < count; i++)
	{
		word = rand() % N;
		printWord(pwords, word);
		
		tmp = pwords[N-1];
		pwords[N-1] = pwords[word];
		pwords[word] = tmp;

		N--;
	}

	
	return 0;
}

int getWords(const char *str, char *p[])
{
	int inWord = 0;
	int count = 0;
	int n = 0, i = 0;

	while (*str != '\0')
	{
		if (*str != ' ' && inWord == 0)
			count++;

		if (*str != ' ' && inWord == 0)
		{
			p[n++] = str;
			inWord = 1;
			
		}
		else if (*str == ' ' && inWord )
		{
			inWord = 0;
		}

		if (*str != ' ' && inWord == 0)
			count++;

		str++;
	}
	
	return count;
}

int printWord(const char *ps[], int k)
{
	
	while (*(ps[k]) != ' ' && *(ps[k])!= '\0')
	{
		putchar(*(ps[k]));
		*(ps[k])++;
	}

	putchar('\0');
			
	return 0;

}