/*-----------------
Практикум 4, задание 3
------------------*/

#include <stdio.h>
#include <string.h>

int main()
{
	char str[256];
	char *p = str, *q = str;
	
	printf("Enter a string:\n");
	fgets(str, 256, stdin);

	while (*q++);
	q -= 3;

	while (p <= q)
	{
		if (*p != *q) 
		{
			printf("No\n");
			return 0;
		}
		p++;
		q--;
	}
	printf("Yes\n");
	return  0;
}