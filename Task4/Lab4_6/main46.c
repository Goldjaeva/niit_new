/*-----------------
Практикум 4, задание 6
------------------*/

#include <stdio.h>
#include <string.h>
#include <limits.h>
int main()
{
	char names[256][20];
	char *old, *young;
	int count = 0, i = 0;
	int max = 0, min = INT_MAX;
	int age = 0;
	
	printf("Enter a number of your reletives: ");
	if (scanf("%d", &count) != 1)
	{
		printf("Incorrect input!\n");
		return 1;
	}

	for (i = 0; i < count; i++)
	{
		printf("Enter a name: ");
		if (scanf("%s", names[i]) != 1)
		{
			printf("Incorrect input!\n");
			return 1;
		}
		
		printf("Enter an age: ");
		if (scanf("%d", &age) != 1)
		{
			printf("Incorrect input!\n");
			return 1;
		}
		
		if (age > max)
		{
			old = names[i];
			max = age;
		}
		if (age < min)
		{
			young = names[i];
			min = age;
		}
	}

	printf("The youngest: %s, age: %d\n", young, min);
	printf("The oldest: %s, age: %d\n", old, max);
	return 0;
}