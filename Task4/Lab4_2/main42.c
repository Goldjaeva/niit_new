/*-----------------
Практикум 4, задание 2
------------------*/

#include <stdio.h>
#include <string.h>

int main()
{
	char str[256];
	char *pwords[256] = {0}, *p = str;
	int n = 0, i = 0, inWord = 0;

	printf("Enter a string:\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = '\0';

	while (*p != '\0')
	{
		if (*p != ' ' && inWord == 0)
		{
			pwords[n++] = p;
			inWord = 1;
		}
		else if (*p == ' ' && inWord )
		{
			inWord = 0;
			p[i] = '\0';

		}
		p++;
	}
	

	for (i = n - 1; i >= 0; i--)
	{
		printf("%s ", pwords[i]);
	}
	return 0;
}