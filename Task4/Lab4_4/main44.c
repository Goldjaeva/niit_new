/*-----------------
Практикум 4, задание 4
------------------*/

#include <stdio.h>
#include <string.h>

int main()
{
	char str[256];
	char *pMax = 0, *p = str;
	int maxLen = 1, len = 1;

	printf("Enter a string:\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = '\0';

	while (*p)
	{
		
		if (*p == *(p+1))
		{
			len++;
		}
		
		if (len > maxLen)
		{
			maxLen = len;
		}

		if (*p != *(p+1))
		{
			len = 1;
			
		}
			p++;
	}

	printf("Max lenght of substring: %d, substring %s", maxLen, *pMax);
	return 0;
}